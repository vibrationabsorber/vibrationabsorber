#ifndef BASE_PARSER_H
#define BASE_PARSER_H

#include <string>
#include <sstream>
#include <stdexcept>

template < class T >
std::string ToString(const T &arg) {
	std::ostringstream	out;
	out << arg;
	return out.str();
}
//������ ������������ ����������������� ����� (����������� �����)
class ConfigParser{
public:
	class ConfigParseError: public std::logic_error{
		int m_line;
		static std::string m_action;
	public:
		ConfigParseError(const std::string& msg, int line): std::logic_error(msg), m_line(line){}
		int line() const {return m_line;}
		std::string info() const;
		static void setAction(const std::string& action){m_action = action;}
		static std::string action(){return m_action;}
	};
	ConfigParser(): m_line(1), m_col(1), m_spaces(" \n\t"), m_braces("()[]"), m_delim(":="), m_hex_char("abcdefABCDEF"),
	m_letters("abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ"), m_digits("0123456789"){}
	virtual ~ConfigParser(){}
	std::string tokenStream(std::string text);//debug: ������ ���� ����� �������
	void parseConfig(std::string text);
protected:
	typedef enum{ttEof, ttId, ttLeftBrace, ttRightBrace, ttLeftSquareBrace, ttRightSquareBrace,
		ttNamespace, ttEqual, ttString, ttNumber, ttFloatNumber, ttError} TokenType;
	struct Token{
		TokenType type;
		std::string text;
		Token(TokenType _type = ttId, std::string _text = ""): type(_type), text(_text){}
	};
	Token m_token;//�������������� �����
	int m_line, m_col;//����� ������ � ������� �������� �������
	virtual void parseConfigParameter(std::string group_name) = 0;
	Token nextToken();
	inline void act(const std::string& c){ConfigParseError::setAction(c);}
	inline void check(Token token, TokenType type);
	inline bool match(Token token, TokenType type){return token.type == type;}
	inline double fetchFloat(){
		check(nextToken(), ttFloatNumber);
		return strtod(m_token.text.c_str(), 0);
	}
	inline size_t fetchNumber(){
		check(nextToken(), ttNumber);
		return strtoul(m_token.text.c_str(), 0, 0);
	}
	inline std::string fetchString(){
		check(nextToken(), ttString);
		return m_token.text;
	}
private:
	const std::string m_spaces;
	const std::string m_braces;
	const std::string m_letters;
	const std::string m_digits;
	const std::string m_delim;
	const std::string m_hex_char;
	std::string m_text;//���������� ����� �����
	std::string::iterator m_cursor;
	//�������������� ����������� �������
	void skipSpaces();
	void skipComments();
	bool iterate();
	inline bool isEof() const {return m_cursor == m_text.end();}
	inline bool isSpace(char t) const {return m_spaces.find(t) != std::string::npos;}
	inline bool isBrace(char t) const {return m_braces.find(t) != std::string::npos;}
	inline bool isLetter(char t) const {return m_letters.find(t) != std::string::npos;}
	inline bool isDigit(char t) const {return m_digits.find(t) != std::string::npos;}
	inline bool isSign(char t) const {return std::string("-+").find(t) != std::string::npos;}
	inline bool isDelim(char t) const {return m_delim.find(t) != std::string::npos;}
	inline bool isHexDigit(char t) const{return  isDigit(t) || (m_hex_char.find(t) != std::string::npos);}
};

#endif //BASE_PARSER_H