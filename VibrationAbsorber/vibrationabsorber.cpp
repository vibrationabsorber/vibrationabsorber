#include "vibrationabsorber.h"

static CC const& Par = config.getCommon();

VibrationAbsorber::VibrationAbsorber(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
	, m_status(s_Initial)
{
	setWindowTitle(tr("��������� ��������� ��������� ���������"));

	//! ���� � ������������� ����������� ���������
	QGridLayout* param_layout = new QGridLayout;

	param_layout->addWidget(new ParamName(tr("������������ ��������: ")), 0, 0);
	param_layout->addWidget(m_absorber_name = new Title(tr(""), 13), 0, 1);

	param_layout->addWidget(new ParamName(tr("����� ���������: ")), 1, 0);
	param_layout->addWidget(m_total_time = new TDisplayField("", tr("�.:�.:�.")), 1, 1);
	param_layout->addWidget(new ParamName(tr("����� � ������ ���������: ")), 2, 0);
	param_layout->addWidget(m_time = new TDisplayField("", tr("�.:�.:�.")), 2, 1);

	param_layout->addWidget(new ParamName(tr("������������ ���� ��� ������: ")), 3, 0);
	param_layout->addWidget(m_max_press_power = new TDisplayField("", tr("�H")), 3, 1);
	param_layout->addWidget(new ParamName(tr("������������ ���� ��� �����: ")), 4, 0);
	param_layout->addWidget(m_max_strain_power = new TDisplayField("", tr("�H")), 4, 1);

	param_layout->setRowStretch(5, 1);

	QHBoxLayout* top_layout = new QHBoxLayout;
	top_layout->addStretch(1);
	top_layout->addLayout(param_layout);
	top_layout->addSpacing(30);
	top_layout->addWidget(m_graph = new Oscilloscope(800), 0, Qt::AlignRight);
	top_layout->addStretch(1);
	//! ������������� ��������� ������������ ����� - ��������� �� ����������������� �����
	m_graph->setCoord(Par.move_max, Par.power_max, Par.move_step, Par.power_step);

	//! ������ ���������� ����������
	QStringList buttons = (QStringList() 
		<< tr("���������") 
		<< tr("����������")
		<< tr("������������ ��������") 
		<< tr("�����"));

	const int btn_sz = buttons.count();
	MainTestButton** pbutton = new MainTestButton*[btn_sz];
	for(int i = 0; i < btn_sz; i++)
	{
		pbutton[i] = new MainTestButton(buttons.at(i), 0);
		pbutton[i]->setFixedSize(QSize(150, 50));
	}
	QHBoxLayout* btn_layout = new QHBoxLayout;
	btn_layout->addStretch(1);
	for(int i = 0; i < btn_sz; i++)
	{
		btn_layout->addWidget(pbutton[i]);
	}
	btn_layout->addStretch(1);
	btn_layout->setAlignment(Qt::AlignCenter);

	//! ���������� � ������� �����������
	//! ������ ���������
	connect(pbutton[0], SIGNAL(pressed(int)), SLOT(startButtonPressed()));
	//! ������� ���������
	connect(pbutton[1], SIGNAL(pressed(int)), SLOT(stopButtonPressed()));
	//! ������������ ��������
	connect(pbutton[2], SIGNAL(pressed(int)), SLOT(protocolButtonPressed()));
	//! �����
	connect(pbutton[3], SIGNAL(pressed(int)), SLOT(exit()));

	//! ���������� ���� �����
	QVBoxLayout* main_layout = new QVBoxLayout;
	main_layout->addStretch(1);
	//! ���������
	main_layout->addWidget(m_title = new TestTitle(tr("����� ��������� ��������� ���������")));
	main_layout->addSpacing(50);
	//! ������������ ��������� � ������
	main_layout->addLayout(top_layout);
	main_layout->addSpacing(50);
	//! ������
	main_layout->addLayout(btn_layout);
	main_layout->addStretch(2);

	//! ��������� ������������ ������� ����������
	QWidget* central_wgt = new QWidget;
	central_wgt->setLayout(main_layout);
	setCentralWidget(central_wgt);

	//! ������� ���� ���������
	m_menu_bar = new QMenuBar;
	m_file_menu = m_menu_bar->addMenu(tr("���������"));
	m_aNewProtocol = m_file_menu->addAction(tr("����� ���������"));
	m_aOpenProtocol = m_file_menu->addAction(tr("������� ��������"));
	m_aEditDatabase = m_file_menu->addAction(tr("���� ������ ���������"));
	m_aExit = m_file_menu->addAction(tr("�����"));
	setMenuBar(m_menu_bar);
	connect(m_aNewProtocol, SIGNAL(triggered()), SLOT(newProtocol()));
	connect(m_aOpenProtocol, SIGNAL(triggered()), SLOT(openProtocol()));
	connect(m_aEditDatabase, SIGNAL(triggered()), SLOT(openDatabase()));
	connect(m_aExit, SIGNAL(triggered()), QCoreApplication::instance(), SLOT(quit()));

	//! ������� �������� ���� - ������ ����� ���
	central_wgt->setBackgroundRole(QPalette::Window);
	central_wgt->setAutoFillBackground(true);
	QPalette p = palette();
	p.setBrush(QPalette::Window, QBrush(Qt::lightGray));
	central_wgt->setPalette(p);

	//! ������ ����� ������������� ������������ ������
	m_protocol_dialog = new ProtocolDialog(this);
	m_msg = new QMessageBox(QMessageBox::Information, tr("��������� �������� ���������"), "", QMessageBox::NoButton, this);

	//! ������ ��� ��������� � �������������� ��
	m_database_dialog = new Database(this);
	connect(m_database_dialog, SIGNAL(absorberChoosed(const AbsorberOptions&)), SLOT(setAbsorberOptions(const AbsorberOptions&)));

	//! ������� ������� ��� ������ � Ltr-��������
	m_ltr41 = new Ltr41Module(this, Par.ltr41_slot);
	m_ltr42 = new Ltr42Module(this, Par.ltr42_slot);
	m_ltr212 = new Ltr212Module(this, Par.ltr212_slot);
	
	//! ������� ������ ��� ������ ������ ltr41
	m_ltr41_reader = new ReaderLtr41(m_ltr41, this);
	connect(m_ltr41_reader, SIGNAL(update()), SLOT(handleLtr41Signal()));
	//������� ������ ��� ������ ������ ltr212
	m_ltr212_reader = new ReaderLtr212(m_ltr212, this);
	connect(m_ltr212_reader, SIGNAL(update()), SLOT(displayPoint()));

	//! ��������� �������
	m_timer.setTimeout(Par.total_time);
	m_timer.setTimeStep(1000);
	connect(&m_timer, SIGNAL(updateTimeField()), SLOT(displayTime()));
	connect(&m_timer, SIGNAL(complete()), SLOT(completeTest()));

	//! ��������� ������ ���������
	m_stBar = new QStatusBar;
	m_stBar->setFixedHeight(40);
	m_stBar->setFont(QFont("Times", 15, QFont::Normal));
	setStatusBar(m_stBar);
}
void VibrationAbsorber::login()
{
	//! �������� ������� ������ � LTR-��������
	m_stBar->showMessage(tr("�������� ������������ ������� � �������� ������..."));
	if(!openModules())
	{
		return;
	}

	//! ��������� ����� ������ LTR-41
	Ltr41Run = true;
	m_ltr41_reader->start();

	//! �������� ��������� � ��������� "����������"
	setComplexState(s_Ok);

	//! ���� ������ ��� ���������
	newProtocol();
}

void VibrationAbsorber::startButtonPressed()
{
	//! ��������� ����� ����������� ������ �� ��������� s_Ok
	if(m_status != s_Ok)
	{
		m_msg->setText(tr("��� ������� ��������� �������� ������ ��������� � ��������� \"����������\"!"));
		m_msg->exec();
		return;
	}
	//! ��������� ������ �������
	//m_timer.beginTest();
	//! ������� �� �������� ������ ������
	m_ltr42->writePort(0x5);

	m_graph->beginDraw();
	m_graph->clear();
	
	//��������� ���� ������ � ������ ltr212
	Ltr212Run = true;
	m_ltr212_reader->start();

	setComplexState(s_Run);
}
void VibrationAbsorber::stopButtonPressed()
{
	if(m_status == s_Run || m_status == s_RunMeasurement)
	{
		stopWork();
	}
	setComplexState(s_Stop);
}

void VibrationAbsorber::failureStop()
{
	if(m_status == s_Run || m_status == s_RunMeasurement)
	{
		stopWork();
	}
	setComplexState(s_Crash);
}

void VibrationAbsorber::stopWork()
{
	//! ������������� �������� ������
	m_ltr42->writePort(0);
	//! ��������� ��������� �������
	m_graph->endDraw();
	//! ������������� ������ �������, ���� �� ��� �������
	m_timer.endTest();
	//! ������������� ������ ����� ������ � Ltr-�������
	Ltr212Run = false;
	m_ltr212_reader->wait();
	//! ��������� ��������� ������ � ��������
	mtEncoder.lock();
	encoder_flag = false;
	encoder_end = 0;
	mtEncoder.unlock();
}

void VibrationAbsorber::protocolButtonPressed()
{
	if(m_status != s_Ok)
	{
		m_msg->setText(tr("��� ������ ��������� �������� ������ ��������� � ��������� \"����������\""));
		m_msg->exec();
		return;
	}
	m_protocol.m_time = m_timer.getTime();
	m_graph->saveGraph(m_protocol.m_gFile_name);
	m_protocol.writeProtocol();
}

void VibrationAbsorber::newProtocol()
{
	m_protocol.clear();
	//! ���� ������
	m_protocol_dialog->exec();
	//! ��������� ��������� ������ �� �����. �������
	m_protocol.m_absorber_type = m_protocol_dialog->m_absorber_type;
	m_protocol.m_absorber_num = m_protocol_dialog->m_absorber_num;
	m_protocol.m_train_num = m_protocol_dialog->m_train_num;
	m_protocol.m_operator = m_protocol_dialog->m_operator;
	m_protocol.m_manager = m_protocol_dialog->m_manager;
	//! ������� ������ ������ �������� (��������� ����������� ���������)
	m_graph->clear();
	m_graph->saveGraph(m_protocol.m_gFile_name);
}

void VibrationAbsorber::openProtocol()
{
	QString docs_path = QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation);
	QDir dir(docs_path + QString("\\Reports\\"));
	if(dir.exists()) docs_path += QString("\\Reports\\");
	QString file = QFileDialog::getOpenFileName(0, tr("�������� ��������� ���������"), docs_path, "*.pdf");
	QDesktopServices::openUrl(QUrl::fromLocalFile(file));
}

void VibrationAbsorber::openDatabase()
{
	m_database_dialog->open();
}

void VibrationAbsorber::setAbsorberOptions(const AbsorberOptions& options)
{
	m_absorber_name->setText(options.name);
	m_graph->setAngle(options.angle);
}

// ��������� ������� ��� ������������� ����� � ��������
/*
 ��������:
 1. ���������� �������� ������, ���������� ������ � Ltr-212 � � ��������;
 2. ������������� ����� � ������������ � �������� ��������;
 3. ������� ������ ����� � ������������ �� ����������:
  - ������� ������ ��������;
  - ������� ���������� ��������;
 4. ������� ��������� ��� ���������� ������������ ������� �������� �������� ���������.
*/

// ������������� ������/���������� �������� � ��������� �������
struct PVal
{
	PVal(): press(0), p_cnt(0){}
	double press;   // �������� � �����
	int p_cnt;      // ����� ��������� � ������ �����
};

void VibrationAbsorber::drawGraph()
{
	const int pnt_size = Par.move_value; // ��������� ����������� ������ (� ��.)
	PVal* pos_pnt = new PVal[pnt_size]; // ������
	PVal* neg_pnt = new PVal[pnt_size]; // ����������

	// debug
	//using namespace std;
	//ofstream out("graph.log", ios::out);
	//out << "���/��2 � ltr: " << endl;
	// ������������� ����� � ������������ � �������� ��������
	for(int i = 0; i < press_end; i++)
	{
		press_buf[i] = Par.power_mul*(press_buf[i] - Par.power_offset);
		//out << press_buf[i] << " ";
	}

	double move_step = 0, move_index = 0;
	int start_ptr = 0, ptr = 0; // ��� ��������� ���������� ����� � ������������

	
	//out << endl << endl << "pnt_size = " << pnt_size << endl;

	// ����� ��������� �� ������ �� ������� �������
	// ������� ������� ������� ����� ���������, ����� ������
	while(++ptr < press_end && !(press_buf[ptr - 1] < 0 && press_buf[ptr] > 0));
	start_ptr = ptr;
	while(ptr < press_end)
	{
		// ����� ��� ������������� �������������
		while(++ptr < press_end && !(press_buf[ptr - 1] > 0 && press_buf[ptr] < 0));
		if(ptr == press_end) break;
		// ���������� ������������ ����� ������������ ��� �������
		move_step = (double)pnt_size / (ptr - start_ptr);
		move_index = 0;
		//out << "point cnt: " << (ptr - start_ptr) << ", move_step: " << move_step;
		//out << ", positiv vals: " << endl;;
		for(int i = start_ptr; i < ptr; i++)
		{	
			//out << press_buf[i] << " ";
			pos_pnt[(int)floor(move_index)].press += press_buf[i];
			pos_pnt[(int)floor(move_index)].p_cnt++;
			move_index += move_step;
		}
		//out << endl << endl;
		start_ptr = ptr;
		// ����� ��� ������������� �������������
		while(++ptr < press_end && !(press_buf[ptr - 1] < 0 && press_buf[ptr] > 0));
		if(ptr == press_end) break;
		// ���������� ������������ ����� ������������ ��� �������
		move_step = (double)pnt_size / (ptr - start_ptr);
		move_index = 0;
		//out << "point cnt: " << (ptr - start_ptr) << ", move_step: " << move_step;
		//out << ", negativ vals: " << endl;
		for(int i = start_ptr; i < ptr; i++)
		{	
			//out << press_buf[i] << " ";
			neg_pnt[(int)floor(move_index)].press += press_buf[i];
			neg_pnt[(int)floor(move_index)].p_cnt++;
			move_index += move_step;
		}
		//out << endl << endl;
		start_ptr = ptr;
	}
	// ��������� ����������� �������� � ������
	//out << "Avg positiv: " << endl;
	for(int i = 0; i < pnt_size; i++)
	{
		if(pos_pnt[i].p_cnt > 1) pos_pnt[i].press = pos_pnt[i].press / pos_pnt[i].p_cnt;
		if(neg_pnt[i].p_cnt > 1) neg_pnt[i].press = neg_pnt[i].press / neg_pnt[i].p_cnt;

		//out << pos_pnt[i].press << " ";
	}
	//out << endl;
	// ����������� �������� (����� ������ ��������� ��������)
	for(int i = 1; i < pnt_size - 1; i++)
	{
		if(pos_pnt[i].press > pos_pnt[i-1].press && pos_pnt[i].press > pos_pnt[i+1].press || 
		   pos_pnt[i].press < pos_pnt[i-1].press && pos_pnt[i].press < pos_pnt[i+1].press)
		   pos_pnt[i].press = (pos_pnt[i-1].press + pos_pnt[i+1].press) / 2;
	}
	// ������� ������ �� �����
	m_graph->beginDraw();
	//out << endl << "Points:" << endl;
	for(int i = 0; i < pnt_size; i++)
	{
		if(pos_pnt[i].p_cnt > 0)
		{
			move_index = i - Par.half_move_value + 0.5;
			m_graph->addPoint(move_index, pos_pnt[i].press);
			//out << move_index << ", " << pos_pnt[i].press << endl;
		}
	}
	//out << endl;
	for(int i = 0; i < pnt_size; i++)
	{
		if(neg_pnt[i].p_cnt > 0)
		{
			move_index = Par.half_move_value - i - 0.5;
			m_graph->addPoint(move_index, neg_pnt[i].press);
			//out << move_index << ", " << neg_pnt[i].press << endl;
		}
	}
	// �������� ������
	m_graph->addPoint((double)-Par.half_move_value + 0.5 , pos_pnt[0].press);
	//out << (double)-Par.half_move_value + 0.5 << ", " << pos_pnt[0].press << endl;
	m_graph->update();

	//out.close();

	delete pos_pnt;
	delete neg_pnt;
}

void VibrationAbsorber::completeTest()
{
	m_ltr42->writePort(0);
	Ltr212Run = false;
	//! ���� ��������� ������
	m_ltr212_reader->wait();
	press_flag = false;
	encoder_flag = false;

	//! ���������� ��������� - ������
	drawGraph();

	m_msg->setText(tr("��������� �������� ��������� ���������.\n��� ��������� ��������� ������� ������ \"������������ ��������\"."));
	m_msg->exec();
	setComplexState(s_Complete);
}
void VibrationAbsorber::exit()
{
	//! ������������� ������, ���� ��� ��������
	Ltr41Run = false;
	Ltr212Run = false;
	m_ltr41_reader->wait();
	m_ltr212_reader->wait();
	//! �������� ������� ������ � ��������
	closeModules();
	QCoreApplication::quit();
}
bool VibrationAbsorber::openModules()
{
	m_ltr41->open();
	m_ltr42->open();
	m_ltr212->open();

	if( !(m_ltr41->isOpen() && m_ltr42->isOpen() && m_ltr212->isOpen()) )
	{
		std::stringstream msg;
		msg << "�� ������� ������� ����� ����� � �������� ������ (" 
			<< (!m_ltr41->isOpen() ? " LTR-41 " : "")
			<< (!m_ltr42->isOpen() ? " LTR-42 " : "")
			<< (!m_ltr212->isOpen() ? " LTR-212 " : "")
			<< ")." << std::endl
			<< "���������, ��� ����� ��������� ��������� � ���������� � ������������� ���������.";
		m_msg->setText(tr(msg.str().c_str()));
		m_msg->exec();
		setComplexState(s_Crash);
		return false;
	}
	return true;
}
void VibrationAbsorber::closeModules()
{
	if(m_ltr41->isOpen()) m_ltr41->close();
	if(m_ltr42->isOpen()) m_ltr42->close();
	if(m_ltr212->isOpen()) m_ltr212->close();
	//if(m_log.is_open()) m_log.close();
}

/*
-In1  � ������ ��������� ������� ����� ������;
-In2  � ������ ������� ���������� ������ ����������;
-In3  � ������ ������� ���������� ������ �����;
-In4  � ������ ������� ���������� ������ ��������;
-In5  � ������ ������� ���������� ������ ������ �����;
-In6  � ������ ������� ���������� ������ ������ �����;
-In7  � ������ �������� 2^0;
-In8  � ������ �������� 2^1;
-In9  � ������ �������� 2^2;
-In10 � ������ �������� 2^3;
-In11 � ������ �������� 2^4;
-In12 � ������ �������� 2^5;
-In13 � ������ �������� 2^6;
-In14 � ������ �������� 2^7;
-In15 � ������ �������� 2^8;
-In16 � ������ �������� 2^9;
*/

void VibrationAbsorber::handleLtr41Signal()
{
	mutInput.lock();
	const WORD signal = input;
	mutInput.unlock();
	
	//! ������ ��������� ������� ����� - ������ ������ ����
	const bool cmd_power = (signal & (1 << Par.in_cmd_power)) != 0;
	//! ���� ������� ��� - ��������� ���������
	//! ��� ���� �� �����, � ����� ��������� ��������� �������� � ������ ������
	if(!cmd_power)
	{
		failureStop();
		return;
	}

	//! ������� �� ����� ���������
	const bool cmd_start = (signal & (1 << Par.in_cmd_start)) != 0;
	//! ������� �� ���� ���������
	const bool cmd_stop = (signal & (1 << Par.in_cmd_stop)) != 0;
	//! ������� �� ��������� ����� � �������� � ����������� ������
	const bool cmd_measure = (signal & (1 << Par.in_cmd_measure)) != 0;

	switch(m_status)
	{
	case s_Initial:
		break;
	case s_Ok:
		//! ������ ���������
		if(cmd_start)
		{
			startButtonPressed();
		}
		break;
	case s_Run:
		//! ������� ���������
		if(cmd_stop)
		{
			stopButtonPressed();
		}
		//! ������ ���������
		else if(cmd_measure)
		{
			//! �������� ��������� ������ � ������� ��������
			press_end = 0;
			press_flag = true;
			setComplexState(s_RunMeasurement);
			//! ������ ����������� �����, � ������� �������� ����������� ���������
			m_timer.beginTest();
		}
		break;
	case s_RunMeasurement:
		//! ������� ���������
		if(cmd_stop)
		{
			stopButtonPressed();
		}
		break;
	case s_Stop:
	case s_Complete:
	case s_Crash:
		//! ���� ������� ����, �� ��������� � ��������� ����������
		if(cmd_power)
		{
			setComplexState(s_Ok);
		}
		break;
	}
}

void VibrationAbsorber::displayPoint()
{
	//m_move->setText(QString::number(move_shatun, 'f', 1));
	//m_power->setText(QString::number(power_shatun, 'f', 1));
	m_max_press_power->setText(QString::number(max_press_power, 'f', 1));
	m_max_strain_power->setText(QString::number(max_strain_power, 'f', 1));
}

void VibrationAbsorber::displayTime()
{
	m_total_time->setText(m_timer.getTimeout());
	m_time->setText(m_timer.getTime());

	//! ���� ������ � ������� �������� ����������� �� �������
	/*if(m_timer.secs() == Par.graph_time)
	{
		//! �������� ��������� ������ � ��������
		mtEncoder.lock();
		press_end = 0;
		press_flag = true;
		mtEncoder.unlock();
	}*/
}

void VibrationAbsorber::setComplexState(VibrationAbsorber::Status status)
{
	switch(status)
	{
		case s_Ok:
			m_stBar->showMessage(tr("�������� ����� � ���������")); 
			break;
			
		case s_Run: 
			m_stBar->showMessage(tr("����������� ��������� - ������� �����")); 
			break;

		case s_RunMeasurement: 
			m_stBar->showMessage(tr("��������� ���������� ��������")); 
			break;

		case s_Stop: 
			m_stBar->showMessage(tr("��������� �����������")); 
			break;

		case s_Complete: 
			m_stBar->showMessage(tr("��������� �������� ���������")); 
			break;

		case s_Crash: 
			m_stBar->showMessage(tr("��������� ����. ��������� ������� ����� ������")); 
			break;

		default:
			m_stBar->showMessage(tr("")); 
	}
	m_status = status;
}
