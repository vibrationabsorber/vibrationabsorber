#include "LtrModules.h"

//��������� ��� ������ � �������� ���������� � ������������ ���������� ��� ������� ����������� ������
//������������ � ������ �� ������� ����� �������� ������ ���� �����
QMutex Ltr41Mod, Ltr42Mod, Ltr212Mod;
//���� - ������� ��������������� ����� ��� ���
volatile bool Ltr41Run = false, Ltr42Run = false, Ltr212Run = false;
//�����, ��������� � Ltr41
WORD input = 0;
QMutex mutInput;

//-------------------------------------------------------------------------------------
//������ Ltr41
Ltr41Module::Ltr41Module(QObject* parent, unsigned int slot): QObject(parent), m_slot(slot)
{
	m_error = LTR41_Init(&m_ltr);
}
bool Ltr41Module::open()
{
	Ltr41Mod.lock();
	m_error = LTR41_Open(&m_ltr, SADDR_DEFAULT, SPORT_DEFAULT, "", m_slot);
	//if(m_error != LTR_OK)
	//	print(tr("������. �� ������� ������� ����� ����� � Ltr41"));
	m_ltr.StreamReadRate = 1000;//������� ���������� ����� ������ � ������
	//
	m_error = LTR41_Config(&m_ltr);
	//if(m_error != LTR_OK)
	//	print(tr("������. �� ������� ���������������� ����� ����� � Ltr41"));
	Ltr41Mod.unlock();
	return (m_error == LTR_OK);
}
bool Ltr41Module::isOpen()
{
	return LTR41_IsOpened(&m_ltr) == LTR_OK;
}
void Ltr41Module::close()
{
	Ltr41Mod.lock();
	m_error = LTR41_Close(&m_ltr);
	Ltr41Mod.unlock();
}
QString Ltr41Module::getErrorString(INT error)
{
	return tr((const char*)LTR41_GetErrorString(error));
}
WORD Ltr41Module::readPort()
{
	WORD InputData = 0;
	Ltr41Mod.lock();
	LTR41_ReadPort(&m_ltr, &InputData);
	Ltr41Mod.unlock();
	return InputData;
}
void Ltr41Module::readPort(WORD *InputData)
{
	Ltr41Mod.lock();
	LTR41_ReadPort(&m_ltr, InputData);
	Ltr41Mod.unlock();
}
void Ltr41Module::processData(DWORD* src, WORD* dest, DWORD size)
{
	for(DWORD i = 0; i < size; i++)
		dest[i] = (WORD)(src[i] >> sizeof(WORD)*8);
}
//-------------------------------------------------------------------------------------
//������ Ltr42
Ltr42Module::Ltr42Module(QObject* parent, unsigned int slot): QObject(parent), m_slot(slot)
{
	m_error = LTR42_Init(&m_ltr);
}
bool Ltr42Module::open()
{
	Ltr42Mod.lock();
	m_error = LTR42_Open(&m_ltr, SADDR_DEFAULT, SPORT_DEFAULT, "", m_slot);
	Ltr42Mod.unlock();
	return (m_error == LTR_OK);
}
bool Ltr42Module::isOpen()
{
	return LTR42_IsOpened(&m_ltr) == LTR_OK;
}
void Ltr42Module::close()
{
	Ltr42Mod.lock();
	m_error = LTR42_WritePort(&m_ltr, 0);
	m_error = LTR42_Close(&m_ltr);
	Ltr42Mod.unlock();
}
QString Ltr42Module::getErrorString(INT error)
{
	return tr((const char*)LTR42_GetErrorString(error));
}
bool Ltr42Module::writePort(WORD data)
{
	Ltr42Mod.lock();
	m_error = LTR42_WritePort(&m_ltr, data);
	Ltr42Mod.unlock();
	return m_error == LTR_OK;
}
//-------------------------------------------------------------------------------------
//������ Ltr212
Ltr212Module::Ltr212Module(QObject *parent, unsigned int slot): QObject(parent), m_slot(slot)
{
	//�������� ������������� ������ ����� � ������� � 
	//���������� ����� ��������� �������� ������ ���������� �� ���������
	LTR212_Init(&m_ltr);
	m_log.open("Ltr212Log.txt");//debug - ������� ���� ������ ����� ������� ���������
}
//������������� � �������� ������
bool Ltr212Module::open()
{
	Ltr212Mod.lock();
	//�������� ������������� ������ ����� � �������,
	//�������� ����, ���������� ����������� ��������� ��������
	//m_error = LTR212_Open(&m_ltr, SADDR_DEFAULT, SPORT_DEFAULT, "", m_slot, "c:\\ltrlibrary\\lib\\LTR212.BIO");
	m_error = LTR212_Open(&m_ltr, SADDR_DEFAULT, SPORT_DEFAULT, "", m_slot, "LTR212.BIO");
	if(m_error != 0){
		if(m_error == -10)
			print("������. ������������ ����� ��� ������������!");
		print("������. �� ������� ������� ����� ����� � Ltr212");
		print((const char*)LTR212_GetErrorString(m_error));
		Ltr212Mod.unlock();
		return false;
	}
	//�������� ����������� ���������� � ���� ������
	if(LTR212_TestEEPROM(&m_ltr) != LTR_OK)
		print("������ � ���� ������ �� ������ �������� �� �����������!");

	//��� ������� ������ ������ ������ ������������ ��������� ���� ���������
	m_ltr.size = sizeof(TLTR212);
	m_ltr.AcqMode = 1; //����� ����� ������ (4-��������� ������� ��������)
	m_ltr.UseClb = 0;  //���� ������������� ������������� ������������� (������������)
	m_ltr.UseFabricClb = 0; //���� ������������� ��������� ������������� ������������� (�� ������������)
	m_ltr.LChQnt = 1;  //���������� ���������� �������
	m_ltr.LChTbl[0] = LTR212_CreateLChannel(1, 1);//���������� 1-� �����, ���������� +-20V
	m_ltr.REF = 1;
	m_ltr.AC = 0;

	//�������� ��������������� ���������� �� ������
	m_error = LTR212_SetADC(&m_ltr);
	if(m_error != LTR_OK)
		print("������. ��������� � ��������� ���������������� ������!");

	//������� ������ ������ � ������
	//print(QString::number(m_ltr.Fs));
	
	Ltr212Mod.unlock();
	return (m_error == LTR_OK);
}
bool Ltr212Module::isOpen()
{
	return LTR212_IsOpened(&m_ltr) == LTR_OK;
}
void Ltr212Module::close()
{
	Ltr212Mod.lock();
	m_error = LTR212_Close(&m_ltr);
	Ltr212Mod.unlock();
}
QString Ltr212Module::getErrorString(INT error)
{
	return tr((const char*)LTR212_GetErrorString(error));
}
void Ltr212Module::print(const QString& text)
{
	if(m_log) m_log << text.toAscii().data() << std::endl;
}
void Ltr212Module::print(const char* text)
{
	if(m_log) m_log << text << std::endl;
}