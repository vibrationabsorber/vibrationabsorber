#include "params.h"
#include <fstream>//debug

//���������������� ��������� ���������
VibrationAbsorberConfig config;

//������ ���������� ��������� � ����� ������������
void VibrationAbsorberConfig::parseConfigParameter(std::string group_name)
{
	using namespace std;
	check(m_token, ttId);
	std::string id = m_token.text;
	check(nextToken(), ttEqual);
	if(group_name == m_common.name)
	{
		//if(id == "ltr212_slot")		m_common.ltr212_slot = fetchNumber();
		//else if(id == "ltr41_slot")	m_common.ltr41_slot = fetchNumber();
		//else if(id == "Ltr42_slot")	m_common.ltr42_slot = fetchNumber();
		if(id == "total_time"){
			m_common.total_time = fetchNumber();
			m_common.graph_time = ((m_common.total_time > 10) ? m_common.total_time - 10 : 0);
		}
		else if(id == "move_value") 
		{
			m_common.move_value = fetchNumber();
			m_common.half_move_value = m_common.move_value / 2;
			m_common.shatun_move_step = (double)m_common.move_value / m_common.half_encoder_range;
		}
		else if(id == "move_max")	m_common.move_max = fetchNumber();
		else if(id == "move_step")	m_common.move_step = fetchNumber();
		else if(id == "power_max")	m_common.power_max = fetchNumber();
		else if(id == "power_step")	m_common.power_step = fetchNumber();
		else if(id == "power_mul")	m_common.power_mul = fetchFloat();
		else if(id == "power_offset") m_common.power_offset = fetchFloat();
		else if(id == "cmd_power") m_common.in_cmd_power = fetchNumber();
		else if(id == "cmd_start") m_common.in_cmd_start = fetchNumber();
		else if(id == "cmd_stop") m_common.in_cmd_stop = fetchNumber();
		else if(id == "cmd_measure") m_common.in_cmd_measure = fetchNumber();
		else if(id == "log_name")	m_common.log_name = fetchString();
		else if(id == "debug")		m_common.debug = (bool)fetchNumber();
		else
			throw ConfigParseError("� ������ ������ ��� ������ ���������", m_line);
	}
	else
		throw ConfigParseError("������ ���������� � ����� ������ �� ����������", m_line);
}
void VibrationAbsorberConfig::printValue()
{
	using namespace std;
	ofstream out("config_print.txt");
	out << "[" << m_common.name << "]" << endl;
	out << "\t" << "ltr212_slot = " << m_common.ltr212_slot << endl;
	out << "\t" << "ltr41_slot = " << m_common.ltr41_slot << endl;
	out << "\t" << "ltr42_slot = " << m_common.ltr42_slot << endl;
	out << "\t" << "total_time = " << m_common.total_time << endl;
	out << "\t" << "graph_time = " << m_common.graph_time << endl;
	out << "\t" << "move_value = " << m_common.move_value << endl;
	out << "\t" << "half_move_value = " << m_common.half_move_value << endl;
	out << "\t" << "shatun_move_step = " << m_common.shatun_move_step << endl;
	out << "\t" << "move_max = " << m_common.move_max << endl;
	out << "\t" << "move_step = " << m_common.move_step << endl;
	out << "\t" << "power_max = " << m_common.power_max << endl;
	out << "\t" << "power_step = " << m_common.power_step << endl;
	out << "\t" << "power_mul = " << m_common.power_mul << endl;
	out << "\t" << "power_offset = " << m_common.power_offset << endl;
	out << "\t" << "cmd_power = " << m_common.in_cmd_power << endl;
	out << "\t" << "cmd_start = " << m_common.in_cmd_start << endl;
	out << "\t" << "cmd_stop = " << m_common.in_cmd_stop << endl;
	out << "\t" << "cmd_measure = " << m_common.in_cmd_measure << endl;
	out << "\t" << "log_name = " << m_common.log_name << endl;
	out << "\t" << "debug = " << m_common.debug << endl;
	out.close();
}
