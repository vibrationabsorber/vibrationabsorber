#include "testunits.h"

static CC const& Par = config.getCommon();

double move_shatun = 0;
double power_shatun = 0;
double max_press_power = 0;
double max_strain_power = 0;

//! ����� ������ ������, ������� ������� ������ � LTR-212
const int iteration_count = 5;

//! ������� ������ c LTR-212 ���������� 150 �� => 150 ����� �� �������
//! ��������� ���������� ���� �� ��������� � ����������� ����� �����
const DWORD data_sz = 300;
//! to do: LTR212_CalcTimeOut()
const DWORD ltr212_timeout = 2000;
//! ����� ��� ������ ����� ������ c LTR-41
DWORD data[data_sz] = {0};
//! ����� ��� ���������� ������������ ������
double dest[data_sz] = {0.0};

//! ����� ��� ������ ����� (������������?) ������ � LTR-212
double press_buf[iteration_count * 200];
//! ��������� �� ��������� ������� �� ��������� ��������� � ������
int press_end = 0;
bool press_flag = false;

//����� ��� ������ � ��������
const int encoder_sz = iteration_count * 1000;	//� �������
//? encoder_buf - �� ����� 
double encoder_buf[encoder_sz];	//����� ��� ������ � �������� 
WORD encoder_src_buf[encoder_sz];
int encoder_end = 0;			//�������� ������� ���������� ������ ������
bool encoder_flag = false;		//���� ����������, ������������ ������ � �������� ��� ���
QMutex mtEncoder;				//���������� ������ ������� � ������ encoder_buf


void ReaderLtr41::run()
{
	std::ofstream out;
	if(Par.debug) out.open("Ltr41Thread.log", std::ios::out);

	//! ������� ���������� ����� ������ ���������� 1 ���
	//! ������� ������ ������ �� 0.5 �������, ������� 1 �������
	const unsigned int sz = 500;
	const DWORD timeout = 1000;
	//! ����� ��� ������ ����� ������
	DWORD source_points[sz];
	WORD data_points[sz];
	INT error = 0;

	//! ����������� ������ - ������ LTR-41
	Ltr41Mod.lock();
	//! ��������� ������ - ������ � ��������������� ���������
	PTLTR41 pltr = m_ltr->getHandler();

	//! ��������� ��������� ���� ������ � �������� 1 ���
	if((error = LTR41_StartStreamRead(pltr)) != LTR_OK)
	{
		if(out) out << "������. �� ������� ��������� ��������� ���� ������. ��� ������: " << error << std::endl;
	}

	while(Ltr41Run)
	{
		//! ������� ������ ����� ������
		LTR41_Recv(pltr, source_points, 0, sz, timeout);
		//! ��������� ����� ������ => ������ � ���� ���� / BCD (������� �� ������������� ��������)
		m_ltr->processData(source_points, data_points, sz);

		mtEncoder.lock();
		if(encoder_flag)
		{
			// ��������� � ����� ����� � �������� � ���� BCD (���� ?)
			// �������������� � ��. ����������� ��� ����������� �� �������
			for(int i = 0; i < sz; i++)
			{
				//? encoder_end >= encoder_sz
				encoder_src_buf[encoder_end++] = data_points[i];
			}
			if(encoder_end == encoder_sz) 
			{
				encoder_flag = false;
			}
		}
		mtEncoder.unlock();

		//! ��������� ����� � ������� �������� ����������� ��������� - ��������� ������� ���������� ������������ (������ 0.5 ���.)
		mutInput.lock();
		input = data_points[sz - 1];
		mutInput.unlock();

		//! ��������� ����������� �������
		if(out) out << data_points[sz - 1] << std::endl;

		//! ����������� ������ � ����������� (�������������� �� BCD-����)
		//! ����������� ������ ������������ � �������� 7-16 
		const WORD code = data_points[sz - 1] >> 6;
		move_shatun = Par.shatun_position( static_cast<int>(bcd_decode(code)) );
		emit update();
	}
	//! ������������� ��������� ���� ������
	if((error = LTR41_StopStreamRead(pltr)) != LTR_OK)
	{
		if(out) out << "������. �� ������� ���������� ��������� ���� ������. ��� ������: " << error << std::endl;
	}
	Ltr41Mod.unlock();

	if(out.is_open())
	{
		out.close();
	}
}
void ReaderLtr212::run()
{
	std::ofstream out;
	if(Par.debug) out.open("Ltr212Thread.log", std::ios::out);

	PTLTR212 pltr = m_ltr->getHandler();
	//! ��������� ��������� ���� ������
	if(LTR212_Start(pltr) != LTR_OK)
	{
		if(out) out << "������ ��� ������� ����� ������." << std::endl;
		Ltr212Run = false;
	}
	if(out) out << "�������� ���� ������" << std::endl;
	int iter_num = 0;
	while(Ltr212Run)
	{
		//! ������� ��������� ������ ������
		if(LTR212_Recv(pltr, data, NULL, data_sz, ltr212_timeout) != data_sz)
		{
			if(out) out << "������ ��� ����� ������." << std::endl;
			Ltr212Run = false;
		}
		DWORD size = data_sz;
		//! ��������� - �������������� � ������
		if(LTR212_ProcessData(pltr, data, dest, &size, 1) != LTR_OK)
		{
			if(out) out << "������ ��� ��������� ��������� ������." << std::endl;
			Ltr212Run = false;
		}
		// �� ������� ���������� ������ ��� ���������� ���������
		if(press_flag)
		{
			for(int i = 0; i < size; i++)
			{
				press_buf[press_end++] = dest[i];
			}
			// ��� ����������� ������� ���������� 5 ������
			// ���� ������ ������, �� ��������� - ����� ��������
			if(++iter_num == iteration_count)
			{
				press_flag = false;
				iter_num = 0;
			}
		}
		// ������� ���������� ���������� ������
		power_shatun = Par.power_mul*(dest[size - 1] - Par.power_offset);

		// ����� ��������� �� �������� � �����������
		if(max_press_power < power_shatun / 1000)
			max_press_power = power_shatun / 1000;
		if(max_strain_power > power_shatun / 1000)
			max_strain_power = power_shatun / 1000;

		emit update();
	}
	if(out) out << "��������� ���� ������" << std::endl;
	if(LTR212_Stop(pltr) != LTR_OK){
		out << "������ ��� �������� ����� ������." << std::endl;
	}

	if(out.is_open()) out.close();
}
//����������� � ������������� ����� ��� ������ ���� ����
WORD grayencode(WORD g) 
{
    return g ^ (g >> 1);
}
WORD graydecode(WORD gray) 
{
    WORD bin;
    for (bin = 0; gray; gray >>= 1) {
		bin ^= gray;
    }
    return bin;
}

WORD bcd_decode(WORD bcd)
{
	WORD code = ((bcd & 0xf000) >> 12) * 1000;
	code += ((bcd & 0xf00) >> 8) * 100;
	code += ((bcd & 0xf0) >> 4) * 10;
	code += (bcd & 0xf);
	return code;
}

TestTimer::TestTimer(QObject* parent): QTimer(parent), m_time(0, 0)
{
	connect(this, SIGNAL(timeout()), SLOT(updateTime()));
}
void TestTimer::setTimeout(int secs)
{
	m_timeout = QTime(0, 0).addSecs(secs);
	//m_timeout.addSecs(secs);
}
void TestTimer::updateTime()
{
	m_time = m_time.addMSecs(m_timestep);
	emit updateTimeField();
	if(m_time >= m_timeout)
	{
		endTest();
		emit complete();
	}
}
void TestTimer::beginTest()
{
	m_time = QTime(0, 0);
	//emit timeout();
	start(m_timestep);
}
void TestTimer::endTest()
{
	if(isActive()) stop();
}
QString TestTimer::getTime() const
{
	return m_time.toString("hh:mm:ss");
}
QString TestTimer::getTimeout() const
{
	return m_timeout.toString("hh:mm:ss");
}
int TestTimer::secs() const
{
	return m_time.minute()*60 + m_time.second();
}
//-------------------------------------------------------------------------
//�������� ���������
Protocol::Protocol(QObject* parent): QObject(parent), m_num(0)
{
	m_gFile_name = "graph.png";
	
	QDir dir(QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation) + QString("\\Reports\\"));
	if(!dir.exists())
		dir.mkdir(QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation) + QString("\\Reports\\"));
	//������ ���������� ���� (����� ���������)
	std::ifstream in("session");
	if(in.is_open()){
		in >> m_num;
		in.close();
	}
}
void Protocol::clear()
{
	m_time = "0";
	m_coef = "0";
}
void Protocol::saveNum()
{
	std::ofstream out("session");
	out << m_num;
	out.close();
}
void Protocol::writeProtocol()
{
	QString file_name = QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation) + 
		QString("\\Reports\\report") + m_absorber_num + "_" + QDate::currentDate().toString("ddMMyy") + QString(".pdf");
	QPrinter printer;
	printer.setOutputFormat(QPrinter::PdfFormat);
	printer.setOutputFileName(file_name);
	QString protocol_title = tr("�������� �") + QString::number(++m_num) + tr(" �� ") + 
		QDate::currentDate().toString("dd.MM.yyyy") +
		tr("<br>��������� �������� ��������� �") + m_absorber_num + tr(", ��������� ") + m_train_num;// + tr(", ������ ") + m_train_section;
	QTextDocument doc;
	doc.setHtml(
		"<table width='100%'>"
		"<tr><td align='center'><font size='+2'>" + protocol_title + "</font></td></tr>"
		"</table><br><br>"
		//������� ���������� ���������
		"<table width='100%'>" +
		tr("<tr><td><b>����� ���������: </b><br></td><td colspan='3'>") + m_time + "<br></td><td></td><td></td></tr>" +
		//tr("<tr><td><b>2. ����������� �����������: </b></td><td>") + m_coef + "</td></tr>" +
		tr("<tr><td colspan='4' align='center'><b>������ ������ �������� ���������.<b></td></tr>") +
		"<tr><td align='center' colspan='4'><br><img src='" + m_gFile_name + "' width='600'><br></td></tr>"
		"</table><br><br>"
		//�������
		"<table width='100%'>"
		"<tr><td>" + tr("<br><i><b>��������� �����:</b></i> ") + "</td><td><br>" + m_operator + "</td></tr>"
		"<tr><td>" + tr("<br><i>����:  </i> ") + QDate::currentDate().toString("dd.MM.yyyy") + "</td><td>" + tr("<br><i>�������:  ___________</i>") + "</td></tr>"
		"<tr><td>" + tr("<br><i><b>��������� ������:</b></i> ") + "</td><td><br>" + m_manager + "</td></tr>"
		"<tr><td>" + tr("<br><i>����:  </i> ") + QDate::currentDate().toString("dd.MM.yyyy") + "</td><td>" + tr("<br><i>�������:  ___________</i>") + "</td></tr>"
		"</table>"
		);
	doc.print(&printer);
	
	if(!QDesktopServices::openUrl(QUrl::fromLocalFile(file_name))){
		QMessageBox msg;
		msg.setText(tr("�� ���� ������� ") + file_name + QString("!"));
		msg.exec();
	}
}