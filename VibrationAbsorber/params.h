#ifndef PARAMS_H
#define PARAMS_H

#include "BaseParser.h"

//����� ��������� ����������
struct CC{
	CC(const std::string& _name): name(_name), encoder_range(1024), half_encoder_range(512),
	ltr212_slot(1), ltr41_slot(2), ltr42_slot(3), total_time(0), graph_time(0) {}
	std::string name;
	const int encoder_range, half_encoder_range, ltr212_slot, ltr41_slot, ltr42_slot;
	size_t
		total_time,		//����� ������� ��������� (� ��������)
		graph_time, 
		move_value,		//��������� ����������� ������, ������������ (���������)
		move_max,		//������������ ����� �� ����� ����������� ������
		move_step,		//�������� ���� �� ����� ����������� ������
		power_max,		//������������ ����� �� ����� ����������� ����
		power_step;		//�������� ���� �� ����� ����������� ����
	double power_mul,	//��������� ������� ����������� ����
		power_offset;	//�������� ������� ����������� ����
	//! ������ ������ ��� �������. ������
	unsigned in_cmd_power;
	unsigned in_cmd_start;
	unsigned in_cmd_stop;
	unsigned in_cmd_measure;
	std::string log_name;
	bool debug;
	// ������ ����������� ������
	int half_move_value;
	double shatun_move_step;
	double shatun_position(int encoder_value) const
	{
		return (encoder_value < half_encoder_range) ? 
			((double)-half_move_value + shatun_move_step * (double)encoder_value) : 
			((double)half_move_value - shatun_move_step * (encoder_value - half_encoder_range));
	}
};

class VibrationAbsorberConfig: public ConfigParser{
public:
	VibrationAbsorberConfig(): m_common("common"){}
	void printValue();//debug
	//������� ������� � ����������
	CC const& getCommon() const {return m_common;}
protected:
	virtual void parseConfigParameter(std::string group_name);
private:
	CC m_common;
};

//���������� ���������� ����������
extern VibrationAbsorberConfig config;

#endif //PARAMS_H