#include "Database.h"

////////////////////////////////////////////////////////////////////////////////
// ���� ��� �������������� ���� ������

LoginDialog::LoginDialog(QSqlDatabase& db, QWidget *parent, Qt::WFlags flags)
	: QDialog(parent, flags)
	, m_db(db)
	, m_btn_accept(tr("�������"))
	, m_btn_reject(tr("������"))
{
	QGridLayout *glayout = new QGridLayout;
	glayout->addWidget(new QLabel(tr("������:")), 0, 0, Qt::AlignRight);
	glayout->addWidget(m_password = new QLineEdit, 0, 1);
	glayout->addWidget(new QLabel(tr("����� ������:")), 1, 0, Qt::AlignRight);
	glayout->addWidget(m_new_password = new QLineEdit, 1, 1);
	glayout->addWidget(new QLabel(tr("������������� ������:")), 2, 0, Qt::AlignRight);
	glayout->addWidget(m_confirm_new_password = new QLineEdit, 2, 1);
	glayout->addWidget(m_change_password = new QCheckBox(tr("�������� ������")),3,0,1,2, Qt::AlignLeft);

	m_password->setEchoMode(QLineEdit::Password);
	m_new_password->setEchoMode(QLineEdit::Password);
	m_confirm_new_password->setEchoMode(QLineEdit::Password);

	m_new_password->setEnabled(false);
	m_confirm_new_password->setEnabled(false);

	m_change_password->setTristate(false);

	QHBoxLayout *blayout = new QHBoxLayout;
	blayout->addStretch(2);
	blayout->addWidget(&m_btn_accept);
	blayout->addStretch(1);
	blayout->addWidget(&m_btn_reject);
	blayout->addStretch(2);

	QVBoxLayout *layout = new QVBoxLayout;
	layout->addLayout(glayout);
	layout->addStretch(1);
	layout->addLayout(blayout);

	setLayout(layout);
	setWindowTitle(tr("�����������"));
	setMinimumSize(300, 140);

	connect(&m_btn_accept, SIGNAL(clicked()), SLOT(accept()));
	connect(&m_btn_reject, SIGNAL(clicked()), SLOT(reject()));
	connect(m_change_password, SIGNAL(stateChanged(int)), SLOT(enableChangePassword(int)));
}
void LoginDialog::accept()
{
	// ��������� ������, ���� �� ���������, �� ������
	// ���� ���������, �� ������ ��������� ������ ��������, �������������, �������
	if(m_db.isOpen())
	{
		QSqlQuery query(m_db);
		if(!query.exec("SELECT password FROM users WHERE login='admin';"))
		{
			QMessageBox::critical(0, tr("������"), tr("������ ��� ������ ������ �� ������� springs!"));
			clear();
			QDialog::reject();
			return;
		}
		// �������� ������ ������
		query.next();

		if(m_password->text() == query.value(0).toString())
		{
			// ���������, ����� �� ������� ����������� ������
			if(m_change_password->checkState() == Qt::Checked)
			{
				QString new_pass = m_new_password->text().trimmed();
				QString confirm_new_pass = m_confirm_new_password->text().trimmed();
				if(new_pass.isEmpty() || confirm_new_pass.isEmpty())
					QMessageBox::warning(0, tr("������"), tr("�� ������� ������� ������: ������ �� ����� ���� ������!"));
				else if(new_pass != confirm_new_pass)
					QMessageBox::warning(0, tr("������"), tr("�� ������� ������� ������: ������������ ����������!"));
				else
				{
					QSqlQuery q(m_db);
					if(!q.exec(QString("UPDATE users SET password='")+new_pass+QString("' WHERE login='admin';")))
					{
						QMessageBox::warning(0, tr("������"), tr("�� ������� ������� ������: ������ ��� ���������� ������� � ���� ������!"));
					}
					else
						QMessageBox::information(0, tr("���������"), tr("������ ������� �������!"));
				}
			}

			clear();
			QDialog::accept();
			return;
		}
		else
			QMessageBox::warning(0, tr("������"), tr("�������� ������!"));
	}
	else
		QMessageBox::critical(0, tr("������"), tr("��� ������� � ���� ������!"));

	clear();
	QDialog::reject();
}
void LoginDialog::reject()
{
	// ������� ����, ��������� ����, ������ �� ��������
	clear();
	QDialog::reject();
}
void LoginDialog::clear()
{
	m_password->clear();
	m_new_password->clear();
	m_confirm_new_password->clear();
}
void LoginDialog::enableChangePassword(int state)
{
	bool isChange = (state == Qt::Checked);
	if(isChange)
	{
		m_new_password->clear();
		m_confirm_new_password->clear();
	}
	m_new_password->setEnabled(isChange);
	m_confirm_new_password->setEnabled(isChange);
}

///////////////////////////////////////////////////////////////////////////////
// ���� �������������� ���������� ��������

DatabaseEdit::DatabaseEdit(const AbsorberOptions& s, QWidget *parent, Qt::WFlags flags )
	: QDialog(parent, flags)
	, m_btn_save(tr("���������"))
	, m_btn_cancel(tr("������"))
	, m_absorber(s)
	, is_edit(false)
{
	setWindowTitle(tr("��������� ��������"));

	QGridLayout* gl = new QGridLayout;
	
	gl->addWidget(new ParamName(tr("������������ ��������:")), 0, 0);
	gl->addWidget(m_fields[_f_name] = new TEditField(""," ",150),0,1);
	gl->addWidget(new ParamName(tr("���� ������� �������:")), 1, 0);
	gl->addWidget(m_fields[_f_angle] = new TEditField(""," ",150),1,1);

	for(size_t i = 0; i < _f_counts; i++)
		connect(m_fields[i], SIGNAL(textEdited(const QString&)), SLOT(editEvent(const QString&)));

	QHBoxLayout* btn_layout = new QHBoxLayout;
	btn_layout->addWidget(&m_btn_save);
	btn_layout->addWidget(&m_btn_cancel);
	btn_layout->setAlignment(Qt::AlignRight);

	m_btn_save.setEnabled(false);
	m_btn_cancel.setEnabled(true);
	connect(&m_btn_save, SIGNAL(clicked()), SLOT(accept()));
	connect(&m_btn_cancel, SIGNAL(clicked()), SLOT(reject()));

	// ���������� ���� ��������� ����������
	QVBoxLayout* layout = new QVBoxLayout;
	layout->addLayout(gl);         // ���� ��� ��������������
	layout->addLayout(btn_layout); // ������
	setLayout(layout);

	setAbsorber(s);
}

DatabaseEdit::~DatabaseEdit(){}

void DatabaseEdit::accept()
{
	// ��������� ���������� �� ����� � m_absorber
	m_absorber.name = m_fields[_f_name]->text();
	m_absorber.angle = m_fields[_f_angle]->text().toInt();

	clear();
	QDialog::accept();
}

void DatabaseEdit::reject()
{
	// ������� ����
	clear();
	QDialog::reject();
}
void DatabaseEdit::setAbsorber(const AbsorberOptions& a)
{
	m_fields[_f_name]->setText(a.name);
	m_fields[_f_angle]->setText(QString::number(a.angle));
}
void DatabaseEdit::editEvent(const QString& s)
{
	if(!is_edit || !m_btn_save.isEnabled())
	{
		is_edit = true;
		m_btn_save.setEnabled(true);
	}
}
void DatabaseEdit::clear()
{
	for(size_t i = 0; i < _f_counts; i++)
		m_fields[i]->clear();
	m_btn_save.setEnabled(false);
	is_edit = false;
}

///////////////////////////////////////////////////////////////////////////////
// ����� ��� ������ � �� ���������

Database::Database(QWidget *parent, Qt::WFlags flags)
	: QDialog(parent, flags)
	, m_row(-1)
	, m_col(-1)
	, m_db_name("absorber.db")
	, m_db_definition("create_db.sql")
	, m_login(_logoff)
{
	setWindowTitle(tr("���� ��������� ���� ������ ���������"));

	m_db = QSqlDatabase::addDatabase("QSQLITE", "edit_conn");
	m_db.setDatabaseName(m_db_name);

	QStringList headers = (QStringList() 
		<< tr("ID")
		<< tr("������������ ��������")
		<< tr("���� ������� �������") 
		);

	m_table = new QTableWidget(this);
	m_table->setColumnCount(3);
	m_table->hideColumn(0); // ������������� �� ������������
	m_table->setHorizontalHeaderLabels(headers);
	m_table->setWordWrap(true);
	m_table->setEditTriggers(QAbstractItemView::NoEditTriggers);
	m_table->resizeColumnsToContents();

	// ����������� ������: ��������, �������, ���������, ������.
	QStringList buttons = QStringList() 
		<< tr("Login")
		<< tr("��������")
		<< tr("�������������")
		<< tr("�������")
		<< tr("�������")
		<< tr("�������")
		;
	std::vector<QPushButton*> btns;
	for(int i = 0; i < buttons.size(); i++)
		btns.push_back(m_btn[i] = new QPushButton(buttons.at(i)));

	QHBoxLayout* btn_layout = new QHBoxLayout;
	for(int i = 0; i < buttons.size(); i++)
	{
		if(i==1) btn_layout->addStretch(1);
		btn_layout->addWidget(btns[i]);
	}
	btn_layout->setAlignment(Qt::AlignRight);

	connect(btns[_btn_login], SIGNAL(clicked()), SLOT(loginButtonPressed()));
	connect(btns[_btn_add], SIGNAL(clicked()), SLOT(addButtonPressed()));
	connect(btns[_btn_edit], SIGNAL(clicked()), SLOT(editButtonPressed()));
	connect(btns[_btn_delete], SIGNAL(clicked()), SLOT(removeButtonPressed()));
	connect(btns[_btn_choose], SIGNAL(clicked()), SLOT(chooseButtonPressed()));
	connect(btns[_btn_close], SIGNAL(clicked()), SLOT(cancelButtonPressed()));
	

	// ���������� ���� ��������� ����������
	QVBoxLayout* layout = new QVBoxLayout;
	layout->addWidget(m_table);    // �������
	layout->addLayout(btn_layout); // ������
	setLayout(layout);

	connect(m_table, SIGNAL(itemSelectionChanged()), SLOT(selectionChanged()));

	m_table->setMinimumSize(600, 300);
}

Database::~Database()
{}

bool Database::open()
{
	// ���������, ��� �� ����������;
	// ���� ����� ���, �� �������� ������� ��
	if(checkDatabase(m_db_name))
	{
		// ��������� ��
		if(m_db.open())
		{
			// ������ ������ �� ��
			if(readDatabase())
			{
				show();
				return true;
			}else
				QMessageBox::critical(0, tr("������"), tr("��������� ��������� ������ �� ��!"));
		}else
			QMessageBox::critical(0, tr("������"), tr("��������� ������� ���� ��!"));
	}else
		QMessageBox::critical(0, tr("������"), tr("�� �� ����������!"));
	return false;
}
void Database::close()
{
	if(m_db.isOpen())
		m_db.close();
	hide();
}

// ���������, ��� �� ��������� �����������. ���� ��� ��, �� ���������� true
bool Database::checkDatabase(const QString& database_name)
{
	QFile file(database_name);
	if(!file.exists() && !createDatabase(database_name))
	{
		QMessageBox::critical(0, tr("������"), tr("��������� ������� ���� ���� ������!"));
		return false;
	}
	return true;
}


// ��������� ���� ������ �����������
bool Database::createDatabase(const QString& database_name)
{
	QFile file(m_db_definition);
	if(file.open(QIODevice::ReadOnly))
	{
		QList<QByteArray> stmt_list = file.readAll().split(';');

		if(stmt_list.isEmpty())
		{
			QMessageBox::information(0, "", tr("���������� ������� ��: ������ � ���� ����������� ��."));
			return false;
		}
		file.close();

		if(!m_db.open())
			return false;

		QSqlQuery query(m_db);
		for(int i = 0; i < stmt_list.size(); i++)
		{
			query.exec(tr(stmt_list[i]));
		}
		m_db.close();
		
		return true;
	}
	return false;
}


bool Database::readDatabase()
{
	if(!m_db.isOpen()) false;
	QSqlQuery query(m_db);
	if(!query.exec("SELECT * FROM absorbers;"))
	{
		QSqlError e = query.lastError();
		QMessageBox::information(0, tr("������ ��� ���������� SQL-�������"), e.text());
		return false;
	}

	m_row = m_col = -1;
	m_absorbers.clear();
	m_btn[_btn_add]->setEnabled(false);
	m_btn[_btn_edit]->setEnabled(false);
	m_btn[_btn_delete]->setEnabled(false);
	m_btn[_btn_choose]->setEnabled(false);
	//m_btn[_btn_close]->setEnabled(true);
	m_btn[_btn_login]->setText(tr("Login"));
	m_login = _logoff;

	for(int i = 0; i < m_table->rowCount(); i++)
		m_table->removeRow(i);
	m_table->reset();

	while(query.next()) 
	{
		AbsorberOptions a;
		a.id = query.value(0).toInt();
		a.name = query.value(1).toString();
		a.angle = query.value(2).toInt();
		
		m_absorbers.push_back(a);
	}

	// ��������� �������� � m_table
	m_table->setRowCount(m_absorbers.size());
	for(size_t i = 0; i < m_absorbers.size(); i++)
	{
		m_table->setItem(i, 0, new QTableWidgetItem(QString::number(m_absorbers[i].id) ));
		m_table->setItem(i, 1, new QTableWidgetItem(m_absorbers[i].name));
		m_table->setItem(i, 2, new QTableWidgetItem(QString::number(m_absorbers[i].angle) ));
	}
	

	// debug
	//std::ofstream out("sp_list.txt", std::ios::out);
	//for(size_t i = 0; i < m_springs.size(); i++)
	//{
	//	out << m_springs[i];
	//}
	//out.close();

	return true;
}
bool Database::executeQuery(const QString& sql, QSqlDatabase& db)
{
	if(!db.isOpen()) false;
	QSqlQuery query(db);
	if(!query.exec(sql))
	{
		QSqlError e = query.lastError();
		QMessageBox::information(0, tr("������ ��� ���������� SQL-�������"), e.text());
		return false;
	}
	return true;
}
int Database::selectionChanged()
{
	if(!m_btn[_btn_choose]->isEnabled())
	{
		m_btn[_btn_choose]->setEnabled(true);
	}
	return m_row;
}
// �������� ����� ������ � ����. � � ��
void Database::addButtonPressed()
{
	if(!m_db.isOpen()) return;

	AbsorberOptions ao;
	DatabaseEdit se(ao);

	if(se.exec() != QDialog::Accepted)
		return;


	QSqlQuery query(m_db);
	query.exec("SELECT max(id) FROM absorbers;");
	query.next(); // ��������� ������ �� ������ ������

	ao = se.m_absorber;
	ao.id = query.value(0).toInt() + 1;

	QString s_query = ao.insertQuery();
	if(!query.exec(s_query))
	{
		QFile file("error_insert");
		if(file.open(QIODevice::ReadWrite))
		{
			file.write(s_query.toAscii());
			QSqlError e = query.lastError();
			file.write(e.text().toAscii());
			file.close();
		}
		else
			QMessageBox::information(0, "", tr("�� ������� ������� ����"));
		QMessageBox::information(0, "", tr("�� ������� �������� ������ � ��"));
	}
		 
	int row = m_absorbers.size(); // ����� ������ � �������
	m_table->insertRow(row); 
	// ����� �� ��������� item's
	m_table->setItem(row, 0, new QTableWidgetItem(QString::number(ao.id)));
	m_table->setItem(row, 1, new QTableWidgetItem(ao.name));
	m_table->setItem(row, 2, new QTableWidgetItem(QString::number(ao.angle)));
	m_table->setCurrentCell(row, 0);

	m_absorbers.push_back(ao);
}
// �������� ������ ���� ���� �������� ������
void Database::removeButtonPressed()
{
	if(!m_db.isOpen()) return;
	int row = m_table->currentRow();
	if( row >= 0 && row < m_table->rowCount())
	{
		MsgQuestion msg(tr("�������������"), tr("�� ������������� ������ ������� ������?"));
		msg.exec();
		if(msg.clickedButton() == msg.pYes)
		{
			// ������� ������ �� ��
			QSqlQuery query(m_db);
			if(!query.exec(m_absorbers[row].deleteQuery()))
			{
				QMessageBox::information(0, "", tr("�� ������� �������� ������"));
			}
			m_absorbers.erase(m_absorbers.begin() + row);
			// ������� ������ �� ����������� �������������
			m_table->removeRow(row);
		}
	}
}
// �������� ������ ���� ���-�� �������� (��� ��������� ����� ������)
void Database::editButtonPressed()
{
	if(!m_db.isOpen()) return;
	int row = m_table->currentRow();
	if( row >= 0 && row < m_table->rowCount())
	{
		DatabaseEdit se(m_absorbers[row]);		
		if(se.exec() != QDialog::Accepted)
			return;

		AbsorberOptions sp = se.m_absorber;
		QString s_query = sp.updateQuery();
		QSqlQuery query(m_db);
		if(!query.exec(s_query))
		{
			QMessageBox::information(0, "", tr("�� ������� �������� ������"));
			QFile file("error_update");
			if(file.open(QIODevice::ReadWrite))
			{
				file.write(s_query.toAscii());
				QSqlError e = query.lastError();
				file.write(e.text().toAscii());
				file.close();
			}
			else
				QMessageBox::information(0, "", tr("�� ������� ������� ����"));
		}
		m_absorbers[row] = sp;
		m_table->setItem(row, 0, new QTableWidgetItem(QString::number(sp.id)));
		m_table->setItem(row, 1, new QTableWidgetItem(sp.name));
		m_table->setItem(row, 2, new QTableWidgetItem(QString::number(sp.angle)));
	}
}
void Database::chooseButtonPressed()
{
	int row = m_table->currentRow();
	if(row >= 0 && row < m_absorbers.size())
	{
		emit absorberChoosed(m_absorbers[row]);
		cancelButtonPressed();
	}
}

void Database::cancelButtonPressed()
{
	close();
}

void Database::loginButtonPressed()
{
	if(m_login == _logoff)
	{
		// ��������� ������
		LoginDialog login(m_db);
		if(login.exec() == QDialog::Accepted)
		{	
			m_btn[_btn_add]->setEnabled(true);
			m_btn[_btn_edit]->setEnabled(true);
			m_btn[_btn_delete]->setEnabled(true);
			m_btn[_btn_login]->setText(tr("Logoff"));
			m_login = _logon;
		}
	}
	else
	{
		m_btn[_btn_add]->setEnabled(false);
		m_btn[_btn_edit]->setEnabled(false);
		m_btn[_btn_delete]->setEnabled(false);
		m_btn[_btn_login]->setText(tr("Login"));
		m_login = _logoff;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// 

QString AbsorberOptions::insertQuery() const
{
	if(id == 0) return QString("");
	QString query = "insert into absorbers(id, name, angle) "
		"values(" + toString(id) + ",'" + name + "'," + toString(angle) +  ");";
	return query;
}
QString AbsorberOptions::updateQuery() const
{
	if(id == 0) return QString("");

	QString query = "update absorbers set "
		"name = '" + name + "',"
		"angle = " + toString(angle) + 
		" where id = " + toString(id) + ";";
		;
	
	return query;
}
QString AbsorberOptions::deleteQuery() const
{
	if(id == 0) return QString("");
	QString query = "delete from absorbers where id = " + toString(id) + ";";
	return query;
}
QString AbsorberOptions::toString(int i) const
{
	return QString::number(i);
}
QString AbsorberOptions::toString(double d) const
{
	return QString::number(d, 'f', 3);
}
