#ifndef DATABASE_H
#define DATABASE_H

#include <vector>
#include <fstream>
#include <iostream>
#include <QtSql/QtSql>
#include <QtSql/QSQLiteDriver>
#include "widgets.h"
#include "params.h"

// ��������� �������� ���������
struct AbsorberOptions
{
	AbsorberOptions(int id_ = 0): id(id_), name(""), angle(0) {}
	int id;
	QString name; // �������� ��������
	int angle;    // ���� ��������

	QString insertQuery() const;
	QString updateQuery() const;
	QString deleteQuery() const;
	inline QString toString(int) const;
	inline QString toString(double) const;
};

typedef std::vector<AbsorberOptions> AbsorberList;

// ���� ��� �����/����� ������
// ������ ��������� ��� �������������� ��
class LoginDialog: public QDialog
{
	Q_OBJECT
public:
	LoginDialog(QSqlDatabase& db, QWidget *parent = 0, Qt::WFlags flags = 0);
	
public slots:
	virtual void accept();
	virtual void reject();
private:
	QSqlDatabase& m_db;

	QPushButton m_btn_accept;
	QPushButton m_btn_reject;

	QLineEdit *m_password;
	QLineEdit *m_new_password;
	QLineEdit *m_confirm_new_password;
	QCheckBox *m_change_password;

	void clear();
private slots:
	void enableChangePassword(int state);
};

// ���������� ���� ��� �������������� �������� ���������
class DatabaseEdit: public QDialog
{
	Q_OBJECT
public:
	DatabaseEdit(const AbsorberOptions& s, QWidget *parent = 0, Qt::WFlags flags = 0);
	~DatabaseEdit();

	// �������� ���������, ���� ������ ������ ���������
	AbsorberOptions m_absorber;

public slots:
	virtual void accept();
	virtual void reject();
private slots:
	void editEvent(const QString&);
	void setAbsorber(const AbsorberOptions& a);
	void clear();

private:
	// ���� ��� �������������� ���������� ��������
	enum {
		_f_name,
		_f_angle,
		_f_counts
	};
	TEditField* m_fields[_f_counts];
	QPushButton m_btn_save;
	QPushButton m_btn_cancel;
	bool is_edit;
};

// ����� ��� ������ � �� ���������
class Database: public QDialog{
	Q_OBJECT
public:
	Database(QWidget *parent = 0, Qt::WFlags flags = 0);
	~Database();

	// ��������� ������ ��������� �� ���� ������
	bool LoadAbsorberList(AbsorberList& alist);

public slots:
	// ������� ���� �� ��� ��������� � ��������������
	bool open();
	// ������� ���� ��
	void close();

private:
	AbsorberList m_absorbers;
	// ������� � ����������
	QTableWidget* m_table;

	// ����������� ������
	enum {
		_btn_login,
		_btn_add,
		_btn_edit,
		_btn_delete,
		_btn_choose,
		_btn_close,
		_btn_counts
	};

	QPushButton* m_btn[_btn_counts];

	typedef enum{
		_logoff,
		_logon
	} Login;
	// �������-��������� (�������� �� ����)
	Login m_login;

	int m_row, m_col; // �������� ������
	QSqlDatabase m_db;
	QString m_db_name;
	QString m_db_definition;

	bool readDatabase();
	bool createDatabase(const QString& database_name);
	bool checkDatabase(const QString& database_name);

private slots:
	bool executeQuery(const QString& sql, QSqlDatabase& db);

	// ������ ��������
	int selectionChanged();
	// ��������� ������
	void loginButtonPressed();
	void addButtonPressed();
	void editButtonPressed();
	void removeButtonPressed();
	void chooseButtonPressed();
	void cancelButtonPressed();
signals:
	// �� ���� ��������������� - ����� �������� ���� ��������� � �������� ���� ���������
	void updateDatabase();
	void absorberChoosed(const AbsorberOptions&);

};

#endif // DATABASE_H