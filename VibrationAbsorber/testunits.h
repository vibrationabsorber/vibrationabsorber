#ifndef TEST_UNITS_N
#define TEST_UNITS_N

#include <QObject>
#include <QThread>
#include <QMessageBox>//test
#include <QTime>
#include <QPrinter>
#include <QTextDocument>
#include <QTextCursor>
#include <QDesktopServices>
#include <QUrl>
#include <QImage>
#include <cmath>
#include "params.h"
#include "LtrModules.h"

struct Point
{
	Point(double xx = 0.0, double yy = 0.0)
		: x(xx), y(yy)
	{}

	double x;
	double y;
};

//! ����������� � ������������� ����� ��� ������ ���� ����
WORD grayencode(WORD g);
WORD graydecode(WORD gray);

//! ������������� BCD-����
WORD bcd_decode(WORD bcd);

//! ��������� ����������� ������ (���������� ����������)
extern double move_shatun;
//! ��������� ����������� � ������ ���� (���������� ����������)
extern double power_shatun; 

extern double max_press_power;
extern double max_strain_power;

// ����� ���������� ��� ����������� ������������ �������
extern const int encoder_sz;   // ������ ������ ��� ����� � ��������
extern double encoder_buf[];   // ����� ��� ������ � �������� (��������������� ��������)
extern WORD encoder_src_buf[]; // ����� ��� ������ � �������� (�������� � �������� �������)
extern int encoder_end;        // ��������� �� ��������� �������� ����� � ������ ������ ��������
extern bool encoder_flag;      // ���� ���������, ������� �� ������������ ������� � �������� (������ ��� ���������� ���������)
extern QMutex mtEncoder;       // ������� ��� �������� ������� � ��������

extern double press_buf[];     // ����� ��� ������ � ������������
extern int press_end;          // ��������� �� ��������� �������� ����� � ������ ������ ������������
extern bool press_flag;        // ���� - ������� �� ������������ ������� � ������������

//! ��������� ������ ������ � �������� (������ ltr41)
class ReaderLtr41: public QThread
{
	Q_OBJECT
public:
	ReaderLtr41(Ltr41Module* ltr41, QObject* parent = 0)
		: QThread(parent), m_ltr(ltr41){}

	void run();

signals:
	void update();

private:
	Ltr41Module* m_ltr;
};

//! ��������� ������ ������ � ������� �������� (������ ltr212)
class ReaderLtr212: public QThread
{
	Q_OBJECT
public:
	ReaderLtr212(Ltr212Module* ltr212, QObject* parent = 0)
		: QThread(parent), m_ltr(ltr212){}

	void run();

signals:
	void update();

private:
	Ltr212Module* m_ltr;
};

/******************************************************************************
 ���������� ������ - ���������� ����� ���������.
 ��� ����������� �� ������� �������� �����, � ������� �������� ����������� 
 ��������� (������ updateTimeField()) � ����� ��������� ���������.
 �� ��������� ����������� �� ��������� ������� ���������� ������ complete(), 
 ������ ������� ������������.
******************************************************************************/
class TestTimer: public QTimer{
	Q_OBJECT
public:
	TestTimer(QObject* parent = 0);
	//����������� ���������� ������������
	QString getTimeout() const;//inline?
	QString getTime() const;
	int secs() const;
	//��������� ����������
	void setTimeout(int secs);
	void setTimeStep(int msecs = 1000){m_timestep = msecs;}
	void beginTest();	// ������ ������� �������
	void endTest();		// ������� ������� �������
private:
	QTime m_timeout;    // ����� ���������� ���������
	int m_timestep;		// �����, �� ��������� �������� ����������� ��������� �� �������
	QTime m_time;       // �����, ��������� � ������ ���������
private slots:
	void updateTime();  // �������� m_time � ��������� ������� ���������� ���������
signals:
	void updateTimeField();
	void complete();    // ��������� ���������
};

/*
                         �������� � ___ �� __. __. ____�.
��������� �������� ��������� _________ ���������� ________.

����� ���������: 5 ���.

����������� �����������:

������:

��������� ������:  ���                 ___________

��������� ������:  ���                 ___________
*/
class Protocol: public QObject{
	Q_OBJECT
public:
	Protocol(QObject* parent = 0);
	~Protocol(){saveNum();}
	//������������ ������, ��������� �������������
	QString m_absorber_type, m_absorber_num, m_train_num, m_operator, m_manager;
	int m_num;			//����� ���������
	//���������� ���������
	QString m_time, m_coef;
	//����� ����������� ������
	QString m_gFile_name;
	
	void clear();
	void saveNum();
public slots:
	void writeProtocol();
};

#endif //TEST_UNITS_N