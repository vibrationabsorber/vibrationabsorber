#ifndef VIBRATIONABSORBER_H
#define VIBRATIONABSORBER_H

#include <QtGui/QMainWindow>
#include "widgets.h"
#include "testunits.h"
#include "Database.h"

class VibrationAbsorber : public QMainWindow
{
	Q_OBJECT
public:
	VibrationAbsorber(QWidget *parent = 0, Qt::WFlags flags = 0);
	~VibrationAbsorber(){}
	//! C��������, � ������� ����� ��������� ��������
	enum Status 
	{
		//! ��������� ��������� - ������������� ������� ����� � �������� ������, �������� ���������
		s_Initial,
		//! ���������� � ������� ���������
		s_Ok, 
		//! ����������� ��������� (������ � �������� ��� �� �������)
		s_Run, 
		//! ����������� ��������� (��������� ��������)
		s_RunMeasurement,
		//! ��������� ����������� �������������
		s_Stop,
		//! ��������� ��������� �������
		s_Complete,
		//! �������� ������ �� ����������� �������� (��� ������� �� ������)
		s_Crash
	};

public slots:
	void login();
	void startButtonPressed();
	void stopButtonPressed();
	void protocolButtonPressed();
	void exit();
private:
	// ���� ������ ���������
	Database *m_database_dialog;
	// ��������
	Protocol m_protocol;
	ProtocolDialog* m_protocol_dialog;
	TestTitle* m_title;
	QMessageBox* m_msg;
	//! ���� ��� ����������� ������� (����������� ����������� ������ �� ������������ ������)
	Oscilloscope *m_graph;
	//! ������, �������� �� ������ ������� ���������
	TestTimer m_timer;
	//! ������������ ��������, ���������� �� ���� (������)
	Title *m_absorber_name;
	QString m_sTotal_time, m_sTime;

	//! ����, ������������ ��������� ���������
	//! ����� ����� ���������
	TDisplayField *m_total_time;
	//! ������� �����
	TDisplayField *m_time;
	//! ������� ����, ����������� � ������
	TDisplayField *m_power;
	//! ������� ����������� ������
	TDisplayField *m_move;
	TDisplayField *m_max_press_power;
	TDisplayField *m_max_strain_power;

	//! ������ ������������ ���������
	Status m_status;
	//! ������ �������
	QStatusBar *m_stBar;
	//! ������� ���� ���������
	QMenuBar* m_menu_bar;
	QMenu* m_file_menu;
	QAction *m_aNewProtocol, *m_aOpenProtocol, *m_aEditDatabase, *m_aExit;
	
	//! ������� ��� ������ � Ltr-��������
	Ltr41Module* m_ltr41;
	Ltr42Module* m_ltr42;
	Ltr212Module* m_ltr212;
	
	//! �����, ����������� ����� ������ ltr41 (��������� ���������)
	ReaderLtr41* m_ltr41_reader;
	//! �����, ����������� ����� ������ ltr212 (��������� �������� ��������)
	ReaderLtr212* m_ltr212_reader;

	//std::ofstream m_log;

	//! �������� ������������ ������� ����� � LTR-�������� ������
	bool openModules();
	//! �������� ������������ ������� ����� � LTR-�������� ������
	void closeModules();
	//! ���������� ����� ������ ���������
	void setComplexState(Status status);

private slots:
	void handleLtr41Signal();
	void drawGraph();
	void displayPoint();
	void displayTime();
	void completeTest();
	void newProtocol();//������� ������ ��� ���������� ������ ��������� (������ ������ ����������)
	void openProtocol();//������ ��� �������� ����������� ������
	void openDatabase();
	void setAbsorberOptions(const AbsorberOptions&);
	//! ��������� ���� - ����������� ��� ���������� � ������ ���������
	void failureStop();
	void stopWork();
};

#endif // VIBRATIONABSORBER_H
