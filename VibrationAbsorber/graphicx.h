#ifndef GRAPHICX_H
#define GRAPHICX_H

#include <QLabel>
#include <QPalette>
#include <QPainter>
#include <QTimer>
#include <QImage>
#include <cmath>
#include <fstream>//debug
#include "params.h"
#include "testunits.h"

//! debug
extern volatile double X_point;
extern volatile double Y_point;
extern QMutex XY_point;

//! debug: �������� ��������� �����
class PointGenerator: public QObject{
	Q_OBJECT
public:
	PointGenerator(QObject* parent = 0): QObject(parent), m_point_num(0),
		lbound(-60), rbound(60), point_cnt(300)
	{
		m_step = (double)(rbound - lbound)/point_cnt;
	}
	void begin();
	void end();
signals:
	void generated();
private slots:
	void createPoint();
private:
	const int lbound, rbound, point_cnt;
	double m_step;
	double m_x;
	int m_point_num;
	QTimer m_timer;
};

//! ������ ��� ����������� �������
class Oscilloscope: public QLabel
{
	Q_OBJECT
public:
	Oscilloscope(int width, QWidget *parent = 0);
	~Oscilloscope();
	//! ���������� ������������ �����
	//! maxX, maxY - ������������ ��������� �� ����
	//! stepX, stepY - ���� ������� �� ����
	void setCoord(int maxX, int maxY, int stepX, int stepY);
	//! ���������� ������ ������� � ��������
	void setWidth(int width);
	//! ���������� ������ ������� � ��������
	void setHeight(int height);
	//! ���������� ���� �������� ������� ��� ��� ���������
	void setAngle(int angle);
	//! �������� ����������� ������� (�� ��������� ���������)
	void beginDraw();
	//! ��������� ����������� �������
	void endDraw();
	//! ��������� ������ � ����� (����������� ��������)
	void saveGraph(const QString& img_file);
	//! ������� ������
	void clear();
	//! ������ ������ ��� ����� (� �������, ~1 ��� ����������� ������)
	enum {buffer_size = 500000};
protected:
	void drawGraph(QPainter& painter);
	void paintEvent(QPaintEvent*);
private:
	//! ������ � ������ ������� � �������
	int m_width, m_height;
	//! ������ � �������� ������ �������
	int m_phyz_stepX, m_phyz_stepY;

	//! ������������� ��������� ������������ �����
	//! ������������ �������� ������� �� ��������������� ����
	int m_maxX, m_maxY;
	//! ���� ������� �� �����. ��� ��������� (�������/��������)
	int m_stepX, m_stepY;
	//! ���� - ���������� ������ ��� ���
	bool m_isDraw;
	//! ������������ ��������������� �� ��� X � ��� Y ��������������
	double dimX, dimY;
	//! ���� �������� ������� ��������� �� ������� �������.
	//! ��� ���� ������ �������������� ������ ������� �������.
	int m_angle;                    

	//! ������ �����
	QPointF m_points[buffer_size];
	//! ������� ��������� �� ��������� ��������� � ������� �����
	int it_end;

public slots:
	//debug
	void addPoints()
	{
		if(it_end < buffer_size)
		{
			XY_point.lock();
			m_points[it_end].setX(dimX * X_point);
			m_points[it_end].setY(dimY * Y_point);
			XY_point.unlock();
			it_end++;
		}
	}

	inline void addPoint(double x, double y){
		if(it_end < buffer_size)
		{
			m_points[it_end].setX(dimX * x);
			m_points[it_end].setY(dimY * y);
			it_end++;
		}
	}
};

#endif //GRAPHICX_H
