#include "graphicx.h"

//debug
QMutex XY_point;
volatile double X_point = 0;
volatile double Y_point = 0;

Oscilloscope::Oscilloscope(int width, QWidget *parent)
	: QLabel(parent)
	, m_isDraw(false)
	, m_width(width)
	, m_height(width/2)
	, it_end(0)
	, m_angle(0)
{
	setFrameStyle(QFrame::Panel|QFrame::Sunken);
	setLineWidth(2);
	setMidLineWidth(0);
	setFixedSize(m_width, m_height);

	//! ������������� ����� ���
	QPalette pal;
	pal.setColor(QPalette::Background, Qt::gray);
	setPalette(pal);
	setAutoFillBackground(true);

	//! ������������� ������������ ��������� �� ���� (�������� �� ���������)
	m_maxX = 400;
	m_maxY = 8000;

	//! ������������� ���� ������� �� ���� (�������� �� ���������)
	m_stepX = 40;
	m_stepY = 500;
	
	//! ��������� ������ ���� ����� ��������� ��������� � �������� ��� ������ ���
	m_phyz_stepX = m_width/(m_maxX / m_stepX);
	m_phyz_stepY = m_height/(m_maxY / m_stepY);

	//! ����������� ��������������� = ���������� ������ / ���������� ������
	dimX = (double)m_width / m_maxX;
	dimY = (double)m_height/ m_maxY;
}

Oscilloscope::~Oscilloscope()
{
	endDraw();
}

void Oscilloscope::setAngle(int angle)
{
	m_angle = angle;
}

void Oscilloscope::setCoord(int maxX, int maxY, int stepX, int stepY)
{
	m_maxX = maxX;
	m_maxY = maxY;
	m_stepX = stepX;
	m_stepY = stepY;
	dimX = (double)m_width / m_maxX;
	dimY = (double)m_height/ m_maxY;
	m_phyz_stepX = m_width/(m_maxX / m_stepX);
	m_phyz_stepY = m_height/(m_maxY / m_stepY);
	update();
}

void Oscilloscope::setWidth(int width)
{
	m_width = width;
	m_height = width/2;
	setFixedSize(m_width, m_height);
	dimX = (double)m_width / m_maxX;
	dimY = (double)m_height/ m_maxY;
	m_phyz_stepX = m_width/(m_maxX / m_stepX);
	m_phyz_stepY = m_height/(m_maxY / m_stepY);
	update();
}
void Oscilloscope::setHeight(int height)
{
	m_width = 2 * height;
	m_height = height;
	setFixedSize(m_width, m_height);
	dimX = (double)m_width / m_maxX;
	dimY = (double)m_height/ m_maxY;
	m_phyz_stepX = m_width/(m_maxX / m_stepX);
	m_phyz_stepY = m_height/(m_maxY / m_stepY);
	update();
}
void Oscilloscope::beginDraw()
{
	m_isDraw = true;
	it_end = 0;
}
void Oscilloscope::endDraw()
{
	m_isDraw = false;
}
void Oscilloscope::clear()
{
	it_end = 0;
	update();
}
void Oscilloscope::saveGraph(const QString& file)
{
	QImage img(m_width, m_height, QImage::Format_RGB32);
	QPainter painter(&img);
	painter.fillRect(0, 0, m_width, m_height, Qt::white);
	drawGraph(painter);
	img.save(file);
}
void Oscilloscope::paintEvent(QPaintEvent * e)
{
	QPainter painter(this);
	drawGraph(painter);
}
void Oscilloscope::drawGraph(QPainter& painter)
{
	const int left_bound = -m_width/2;
	const int right_bound = m_width/2;
	const int top_bound = m_height/2;
	const int bottom_bound = -m_height/2;
	
	//! ��������� ������ ��������� � ����� �������
	painter.translate(m_width/2, m_height/2);

	//! ���������� ������� ��������� �� ����
	painter.setPen(QPen(Qt::blue, 1));
	painter.drawText(40, -(top_bound - 15), tr("H"));
	painter.drawText((right_bound - 15), -5, tr("��"));
	painter.drawText(-right_bound + 5, -top_bound + 15, tr("�����"));
	painter.drawText(-right_bound + 5, top_bound - 5, tr("������"));

	//! ������ ��� �������, ����� ��� �� ����������������
	painter.setPen(QPen(Qt::black, 1));
	const int stepX_cnt = m_maxX / m_stepX;
	for(int i = 0; i < stepX_cnt; i++)
	{
		painter.drawText(left_bound + i * m_phyz_stepX, -5, "  " + QString::number(-m_maxX/2 + m_stepX * i));
	}
	const int stepY_cnt = m_maxY / m_stepY;
	for(int i = 0; i < stepY_cnt; i++)
	{
		const int n = -m_maxY/2 + m_stepY * i;
		//if((n != 0) && (n % 1000 == 0))
		if(n != 0)
		{
			painter.drawText(5, -(bottom_bound + i * m_phyz_stepY), "  " + QString::number(n));
		}
	}

	painter.scale(-1, 1);
	painter.rotate(180);
	painter.save();

	//! ������ ������������ �����
	painter.setPen(QPen(Qt::darkRed, 1, Qt::DotLine));
	for(int i = left_bound; i < right_bound; i += m_phyz_stepX)
	{
		painter.drawLine(i, bottom_bound, i, top_bound);
	}
	for(int i = bottom_bound; i < top_bound; i += m_phyz_stepY)
	{
		painter.drawLine(left_bound, i, right_bound, i);
	}
	painter.restore();

	//! ������������ ���
	painter.drawLine(left_bound, 0, right_bound, 0);
	painter.drawLine(0, bottom_bound, 0, top_bound);
	//! ���������� ���������� � ����� �� ���� Y � X ��������������
	for(int i = left_bound; i < right_bound; i += m_phyz_stepX)
	{
		painter.drawLine(i, -5, i, 5);
	}
	for(int i = bottom_bound; i < top_bound; i += m_phyz_stepY)
	{
		painter.drawLine(-5, i, 5, i);
	}
	
	if(m_isDraw)
	{
		// debug
		/*std::ofstream out("points.log", std::ios::out);
		if(out.is_open())
		{
			for(int i = 0; i < it_end; i++)
				out << m_points[i].x() << " " << m_points[i].y() << std::endl;
			out.close();
		}*/

		painter.rotate(static_cast<qreal>(m_angle));
		painter.setRenderHint(QPainter::Antialiasing);
		painter.setPen(QPen(Qt::darkBlue, 1));
		painter.drawPolyline(m_points, it_end);
	}
}

//---------------------------------------------------------------------
//debug
void PointGenerator::begin()
{
	m_point_num = 0;
	m_x = lbound;
	//��������� ��������� 10 ����� � �������
	connect(&m_timer, SIGNAL(timeout()), SLOT(createPoint()));
	m_timer.start(100);
}
void PointGenerator::end()
{
	m_timer.stop();
	disconnect(&m_timer, SIGNAL(timeout()), this, SLOT(createPoint()));
}
void PointGenerator::createPoint()
{
	//const int coef = 100;
	if(++m_point_num >= point_cnt){
		end();
		return;
	}
	//static double x = lbound;
	m_x += m_step;
	XY_point.lock();
	X_point = m_x;
	//Y_point = m_x*m_x;
	Y_point = m_x * 50;
	XY_point.unlock();
	emit generated();
}