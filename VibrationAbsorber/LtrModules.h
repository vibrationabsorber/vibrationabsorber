#ifndef LTR_MODULES_H_
#define LTR_MODULES_H_

#include "ltrapidefine.h"
#include "ltrapitypes.h"
#include "ltrapi.h"
#include "ltr41api.h"
#include "ltr42api.h"
#include "ltr212api.h"

#include <fstream>
#include <string>
#include <sstream>
#include <QtGui/QtGui>

extern volatile bool Ltr41Run, Ltr42Run, Ltr212Run;
extern WORD input;
extern QMutex mutInput;
extern QMutex Ltr41Mod;

class Ltr41Module: public QObject{
	Q_OBJECT
public:
	Ltr41Module(QObject* parent = 0, unsigned int slot = 3);
	~Ltr41Module(){
		if(isOpen()) close();
	}
	bool open();
	inline bool isOpen();
	inline void close();
	PTLTR41 getHandler(){return &m_ltr;}
	WORD readPort();
	void readPort(WORD* InputData);
	inline QString getErrorString(INT error);
	void processData(DWORD* src, WORD* dest, DWORD size);
private:
	const unsigned int m_slot;
	TLTR41 m_ltr;
	INT m_error;
};

class Ltr42Module: public QObject{
	Q_OBJECT
public:
	Ltr42Module(QObject* parent = 0, unsigned int slot = 4);
	~Ltr42Module(){
		if(isOpen()) close();
	}
	bool open();
	inline bool isOpen();
	inline void close();
	PTLTR42 getHandler(){return &m_ltr;}
	inline QString getErrorString(INT error);
	bool writePort(WORD data);
private:
	const unsigned int m_slot;
	TLTR42 m_ltr;
	INT m_error;
};
class Ltr212Module: public QObject{
	Q_OBJECT
public:
	Ltr212Module(QObject* parent = 0, unsigned int slot = 1);
	~Ltr212Module(){
		if(isOpen()) close();
		if(m_log.is_open()) m_log.close();
	}
	bool open();
	inline bool isOpen();
	inline void close();
	PTLTR212 getHandler(){return &m_ltr;}
	inline QString getErrorString(INT error);

private:
	const unsigned int m_slot;
	TLTR212 m_ltr;
	INT m_error;
	std::ofstream m_log;
	inline void print(const QString& text);
	inline void print(const char* text);
};

/*
class Ltr11Module: public QObject{
	Q_OBJECT
public:
	Ltr11Module(QObject* parent = 0, unsigned int slot = 2);
	~Ltr11Module(){
		if(isOpen()) close();
	}
	bool open();
	inline bool isOpen();
	inline void close();
	PTLTR11 getHandler(){return &m_ltr;}
	inline QString getErrorString(INT error);
	void setTerminal(QTextEdit* terminal){m_terminal = terminal;}
	inline bool writePort(WORD data);
private:
	const unsigned int m_slot;
	TLTR11 m_ltr;
	INT m_error;
	QTextEdit* m_terminal;
	bool m_isOpen;

	void print(const QString& text);
};

class Ltr34Module: public QObject{
	Q_OBJECT
public:
	Ltr34Module(QObject* parent = 0, unsigned int slot = 1);
	~Ltr34Module(){
		if(isOpen()) close();
	}
	bool open();
	inline bool isOpen();
	inline void close();
	PTLTR34 getHandler(){return &m_ltr;}
	inline QString getErrorString(INT error);
	void setTerminal(QTextEdit* terminal){m_terminal = terminal;}
	inline bool writePort(WORD data);
	QString printConfig();//распечатать содержимое полей описателя
private:
	const unsigned int m_slot;
	TLTR34 m_ltr;
	INT m_error;
	QTextEdit* m_terminal;

	void print(const QString& text);
};*/

#endif //LTR_MODULES_H_