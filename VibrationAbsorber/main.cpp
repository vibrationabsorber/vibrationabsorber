#include "vibrationabsorber.h"
#include <QtGui/QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	QTextCodec::setCodecForTr(QTextCodec::codecForName("Windows-1251"));
	//������ ��������� ��������� �� ����� config.cnf
	std::stringstream s;
	std::ifstream in("config.cnf");
	if(in){
		s << in.rdbuf();
		in.close();
		try{
			config.parseConfig(s.str());
			//config.printValue();//debug
		}catch(const ConfigParser::ConfigParseError& e){
			QMessageBox msg(QMessageBox::Information, QObject::tr("������ � ���������������� �����"), QObject::tr(e.info().c_str()));
			msg.exec();
			exit(1);
		}
	}else{
		QMessageBox msg(QMessageBox::Information, QObject::tr("������"), QObject::tr("�� ������� ������� ���� config.cnf\n����� �������������� �������� ���������� �� ���������."));
		msg.exec();
	}
	VibrationAbsorber w;
	//w.show();
	w.showFullScreen();
	w.login();
	return a.exec();
}
